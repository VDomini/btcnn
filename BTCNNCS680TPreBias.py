import csv
import numpy as np
import random as r
import sys
import matplotlib.pyplot as plt

#Sigmoid function for node values
def g(x):
    return (1 / ( 1 + np.exp(-1 * x)))

#Get input argument for nodes
if len(sys.argv) < 2:
    print("Missing text file with csv file inputs. Exiting.\n")
    exit()
elif len(sys.argv) < 3:
    numHidden = 20
else:
    numHidden = int(sys.argv[2])

infile = open(sys.argv[1], "r")
datafiles = infile.readlines()
alpha = 0.5

#Important variable definitions
numOutputs = 1
ITERS = 100000
HIDDENLAYERS = 5

attList1 = []
r.seed(0)

ficount = 0
#Read in the data from all but the last file.
for fi in datafiles[:-1]:
    #Read in CSV file
    with open(fi.rstrip(), newline='') as csvfile:
        dreader = csv.reader(csvfile, delimiter=',')
        rowc = 0
        for row in dreader:
            if ficount == 0: #Only the first file read will create bias and rows
                ra = [1] #This creates the bias input node
                ra.append(float(row[2]))
                attList1.append(ra)
            else: #The rest merely add on.
                attList1[rowc].append(float(row[2]))
                rowc = rowc + 1
        ficount = ficount + 1

#The last file will be the BTC price file.
with open(datafiles[-1].rstrip(), newline='') as csvfile:
    dreader = csv.reader(csvfile, delimiter=',')
    rowc = 0
    for row in dreader:
        attList1[rowc].append(float(row[3]))
        rowc = rowc + 1
    ficount = ficount + 1

#Send data to a numpy array, which makes my life SO much easier
a1 = np.array(attList1)

#This gets the elements in a random order
eles = r.sample(range(0, len(a1)), len(a1))

#This takes all of the elements up to 2/3rds the length for testing
twothirds = int(np.ceil( 2 * len(eles) / 3))
training = np.take(a1, eles[:twothirds], axis=0)
testing = np.delete(a1, eles[:twothirds], axis=0)


#Standardize both training and testing according to mean and std of training
for x in range(1, len(training[0])-1):
    me = np.mean(training[:,x])
    std = np.std(training[:,x], ddof=1)
    training[:,x] = (training[:,x] - me)/std
    testing[:,x] = (testing[:,x] - me)/std

#Create original weights for input to hidden and hidden to output
i2h = np.empty( [len(training[0])-1, numHidden])
Hiddens = np.empty( [HIDDENLAYERS-1, numHidden, numHidden])
h2o = np.empty( [numHidden, numOutputs])

for x in range(len(i2h)):
    for y in range(len(i2h[0])):
       i2h[x][y] = r.uniform(1, -1)

for x in range(numHidden):
    for y in range(numHidden):
        for z in range(HIDDENLAYERS-1):
            Hiddens[z][x][y] = r.uniform(1, -1)
       
for z in range(len(h2o)):
    h2o[z] = r.uniform(1,-1)


table = []

#Losses listed starting from output
Loss = np.empty( [len(training), numOutputs])

HLoss = np.empty( [HIDDENLAYERS, len(training), numHidden])

Y = training[:,-1*numOutputs]
Y2 = testing[:,-1]

HiddenVals = np.empty( [HIDDENLAYERS, len(training), numHidden])
HiddenValsTesting = np.empty( [HIDDENLAYERS, len(testing), numHidden])

for it in range(ITERS):
    #Now we have the weights, lets get the outputs, o
    HiddenVals[0] = g(np.dot(training[:,:-1*numOutputs],i2h))
    for i in range(1, HIDDENLAYERS):
        HiddenVals[i] = g(np.dot(HiddenVals[i-1], Hiddens[i-1]))
    o = g(np.dot(HiddenVals[-1], h2o))

    #So now we need to update the values

    #Calculate deltas
    for i in range(numOutputs):
        Loss[:,i] = (Y - o[:,i])*o[:,i]*(1 - o[:,i])

    HLoss[0] = np.multiply(np.multiply( np.dot( Loss, h2o.T), HiddenVals[-1]) ,(1 - HiddenVals[-1]))
    for i in range(1, HIDDENLAYERS):
        HLoss[i] = np.multiply(np.multiply( np.dot( HLoss[i-1], Hiddens[-1*i].T), HiddenVals[-1-i]) ,(1 - HiddenVals[-1-i]))

    #Update with Back propagation
    h2o = np.add(np.multiply((alpha / len(training)), np.dot( HiddenVals[-1].T, Loss)), h2o)
    for i in range(len(Hiddens)):
        Hiddens[-1-i] = np.add(np.multiply((alpha / len(training)), np.dot( HiddenVals[-2-i].T, HLoss[i])), Hiddens[-1-i])
    i2h = np.add(np.multiply((alpha / len(training)), np.dot(HLoss[-1].T, training[:,:-1])).T, i2h)

    cor = 0
    incor = 0

    
    #Now we check the correctness of this training pass

    for x in range(len(o[:,0])):
        if o[:,0][x] > 0.5:
            if Y[x] == 1:
                cor = cor + 1
            else:
                incor = incor + 1
        else:
            if Y[x] == 0:
                cor = cor + 1
            else:
                incor = incor + 1

    table.append((1 - (cor / (cor + incor))))

#Get testing error
tcor = 0
tincor = 0

HiddenValsTesting[0] = g(np.dot(testing[:,:-1*numOutputs],i2h))
for i in range(1, HIDDENLAYERS):
    HiddenValsTesting[i] = g(np.dot(HiddenValsTesting[i-1], Hiddens[i-1]))
o = g(np.dot(HiddenValsTesting[-1], h2o))

for x in range(len(o[:,0])):
    print("output is %f actual is %f", o[:,0][x], Y2[x])
    if o[:,0][x] > 0.5:
        if Y2[x] == 1:
            tcor = tcor + 1
        else:
            tincor = tincor + 1
    else:
        if Y2[x] == 0:
            tcor = tcor + 1
        else:
            tincor = tincor + 1

#Plot
print("Testing error is")
print((1 - (tcor / (tcor + tincor))))
plt.plot( range(0,ITERS), table)
plt.xlabel('Iteration')
plt.ylabel('Training Error')
plt.show()
