This code is run using Python 3.5 and has dependencies on the matplotlib and csv libraries.

The number of iterations is controlled by the ITERS variable, the number of hidden layers is controlled by the HIDDENLAYERS variable and the number of hidden nodes is an input parameter.

The code is run by calling python BTCNNCS680T.py FILENAME NUMHIDDENNODES

NUMHIDDENNODES is optional - without an argument, it uses 20.

FILENAME is one of the .txt files included:
BTC.txt
Index.txt
Both.txt

More generally, these files contain a list of input data as csvs whose relevant data is in column B, and the last element is the BTC price csv, whose relevant data is in column C

The output will be a plot showing the error over the iterations on the training set, and then a print out
of the error reported on the testing set by the final trained Neural Network. Significant deviation between
training and testing errors indicates that the Neural Network has been overfit and adjustments need to be made.