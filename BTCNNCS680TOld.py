import csv
import numpy as np
import random as r
import sys
import matplotlib.pyplot as plt


def g(x):
    return (1 / ( 1 + np.exp(-1 * x)))

#Get input argument for nodes
if len(sys.argv) < 2:
    print("Missing text file with csv file inputs. Exiting.\n")
    exit()
elif len(sys.argv) < 3:
    numHidden = 20
else:
    numHidden = int(sys.argv[2])

infile = open(sys.argv[1], "r")
datafiles = infile.readlines()
alpha = 0.5
numOutputs = 1
ITERS = 1000

attList1 = []
r.seed(0)

ficount = 0
#Read in the data from all but the last file.
for fi in datafiles[:-1]:
    #Read in CSV file
    with open(fi.rstrip(), newline='') as csvfile:
        dreader = csv.reader(csvfile, delimiter=',')
        rowc = 0
        for row in dreader:
            if ficount == 0: #Only the first file read will create bias and rows
                ra = [1] #This creates the bias input node
                ra.append(float(row[2]))
                attList1.append(ra)
            else: #The rest merely add on.
                attList1[rowc].append(float(row[2]))
                rowc = rowc + 1
        ficount = ficount + 1

#The last file will be the BTC price file.
with open(datafiles[-1].rstrip(), newline='') as csvfile:
    dreader = csv.reader(csvfile, delimiter=',')
    rowc = 0
    for row in dreader:
        attList1[rowc].append(float(row[3]))
        rowc = rowc + 1
    ficount = ficount + 1

#Send data to a numpy array, which makes my life SO much easier
a1 = np.array(attList1)

#This gets the elements in a random order
eles = r.sample(range(0, len(a1)), len(a1))

#This takes all of the elements up to 2/3rds the length for testing
twothirds = int(np.ceil( 2 * len(eles) / 3))
training = np.take(a1, eles[:twothirds], axis=0)
testing = np.delete(a1, eles[:twothirds], axis=0)


#Standardize both training and testing according to mean and std of training
for x in range(1, len(training[0])-1):
    me = np.mean(training[:,x])
    std = np.std(training[:,x], ddof=1)
    training[:,x] = (training[:,x] - me)/std
    testing[:,x] = (testing[:,x] - me)/std

#Create original weights for input to hidden and hidden to output
i2h = np.empty( [len(training[0])-1, numHidden])
h2o = np.empty( [numHidden, numOutputs])

for x in range(len(i2h)):
    for y in range(len(i2h[0])):
       i2h[x][y] = r.uniform(1, -1)

for z in range(len(h2o)):
    h2o[z] = r.uniform(1,-1)


table = []
Loss = np.empty( [len(training), numOutputs])
Loss2 = np.empty( [len(training), numHidden])
Y = training[:,-1]

for it in range(ITERS):
    #Now we have the weights, lets get the outputs, o
    h = g(np.dot(training[:,:-1],i2h))
    o = g(np.dot(h, h2o))

    #So now we need to update the values

    Loss[:,0] = (Y - o[:, 0])*o[:,0]*( 1 - o[:,0])
    for x in range(len(Loss2)):
        for y in range(len(Loss2[0])):
            Loss2[x][y] = (h2o[y] * Loss[x]) * h[x][y] * ( 1 - h[x][y])

    for x in range(len(h2o[0])):
        h2o[:,x] = np.add(np.multiply((alpha / len(training)), np.dot(Loss.T, h)), h2o[:,x])

    i2h = np.add(np.multiply((alpha / len(training)), np.dot(Loss2.T, training[:,:-1])).T, i2h)

    #get training error
    cor = 0
    incor = 0
    for x in range(len(o[:,0])):
        if o[:,0][x] > 0.5:
            if Y[x] == 1:
                cor = cor + 1
            else:
                incor = incor + 1
        else:
            if Y[x] == 0:
                cor = cor + 1
            else:
                incor = incor + 1

    table.append((1 - (cor / (cor + incor))))

#Get testing error
tcor = 0
tincor = 0

h = g(np.dot(testing[:,:-1],i2h))
o = g(np.dot(h, h2o))
Y = testing[:,-1]

for x in range(len(o[:,0])):
    print("output is %f actual is %f", o[:,0][x], Y[x])
    if o[:,0][x] > 0.5:
        if Y[x] == 1:
            tcor = tcor + 1
        else:
            tincor = tincor + 1
    else:
        if Y[x] == 0:
            tcor = tcor + 1
        else:
            tincor = tincor + 1

#Plot
print("Testing error is")
print((1 - (tcor / (tcor + tincor))))
plt.plot( range(0,ITERS), table)
plt.xlabel('Iteration')
plt.ylabel('Training Error')
plt.show()
